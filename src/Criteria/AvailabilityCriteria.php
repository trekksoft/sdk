<?php
namespace TrekkSoft\SDK\Criteria;

use DateTime;

/**
 * Class AvailabilityCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class AvailabilityCriteria implements MerchantAwareCriteria
{
    use LimitTrait;
    use MerchantTrait;

    /**
     * @var string[]
     */
    private $ids = [];

    /**
     * @var int[]
     */
    private $activityIds = [];

    /**
     * @var string[]
     */
    private $types = [];

    /**
     * @var DateTime
     */
    private $startsFrom = null;

    /**
     * @var DateTime
     */
    private $startsTo = null;

    /**
     * @var DateTime
     */
    private $bookingTime = null;

    /**
     * @var string
     */
    private $ruleId = null;

    /**
     * @var bool
     */
    private $withCustomPriceCategories = false;

    /**
     * @var bool
     */
    private $activeOnly = true;

    /**
     * @var int
     */
    private $seats = null;

    /**
     * @var null|bool
     */
    private $withPriceCategoriesOnly = null;

    /**
     * Activities data to mix into availabilities.
     *
     * Useful in case when you have activities preselected and just want to mix in that data
     * into this availabilities list response
     *
     * @var array
     */
    private $mixInActivities = [];

    private $withExclusive = false;
    private $withCustom = null;
    private $withGuests = false;

    /**
     * @var null|string
     */
    private $language = null;

    /**
     * @var null|DateTime
     */
    private $departureTime = null;

    public function __construct()
    {
        $this->bookingTime = new DateTime();
        $this->startsFrom = new DateTime();
        $this->seats = 1;
        $this->perPage = 1000;
        $this->activeOnly = true;
        $this->withGuests = false;
    }

    /**
     * @param int $activityId
     * @return $this
     */
    public function addActivityId($activityId)
    {
        $activityId = (int)$activityId;
        $this->activityIds[$activityId] = $activityId;
        return $this;
    }

    /**
     * @param int $activityId
     * @return $this
     */
    public function setActivityId($activityId)
    {
        $this->activityIds = [];
        if ($activityId) {
            $this->addActivityId($activityId);
        }
        return $this;
    }

    /**
     * @param int[] $activityIds
     * @return $this
     */
    public function setActivityIds($activityIds)
    {
        $this->activityIds = [];
        foreach ($activityIds as $activityId) {
            $this->addActivityId($activityId);
        }
        return $this;
    }

    /**
     * @return int[]
     */
    public function getActivityIds()
    {
        return array_values($this->activityIds);
    }

    /**
     * @param string $type
     * @return $this
     */
    public function addType($type)
    {
        $this->types[$type] = $type;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param string $availabilityItemId
     * @return $this
     */
    public function addId($availabilityItemId)
    {
        $this->ids[$availabilityItemId] = $availabilityItemId;
        return $this;
    }

    /**
     * @param string $availabilityItemId
     * @return $this
     */
    public function setId($availabilityItemId)
    {
        $this->ids = [];
        if ($availabilityItemId) {
            $this->addId($availabilityItemId);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getIds()
    {
        return array_values($this->ids);
    }

    /**
     * @return DateTime
     */
    public function getStartsFrom()
    {
        return $this->startsFrom;
    }

    /**
     * @param DateTime $startsFrom
     * @return $this
     */
    public function setStartsFrom(DateTime $startsFrom = null)
    {
        $this->startsFrom = $startsFrom;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartsTo()
    {
        return $this->startsTo;
    }

    /**
     * @param DateTime $startsTo
     * @return $this
     */
    public function setStartsTo(DateTime $startsTo = null)
    {
        $this->startsTo = $startsTo;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getBookingTime()
    {
        return $this->bookingTime;
    }

    /**
     * @param DateTime $bookingTime
     * @return $this
     */
    public function setBookingTime(DateTime $bookingTime)
    {
        $this->bookingTime = $bookingTime;
        return $this;
    }

    /**
     * @return $this
     */
    public function resetBookingTime()
    {
        $this->bookingTime = null;
        return $this;
    }

    /**
     * @return string
     */
    public function getRuleId()
    {
        return $this->ruleId;
    }

    /**
     * @param string $ruleId
     * @return $this
     */
    public function setRuleId($ruleId)
    {
        $this->ruleId = $ruleId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWithCustomPriceCategories()
    {
        return $this->withCustomPriceCategories;
    }

    /**
     * @param bool $withCustomPriceCategories
     * @return $this
     */
    public function setWithCustomPriceCategories($withCustomPriceCategories)
    {
        $this->withCustomPriceCategories = $withCustomPriceCategories;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActiveOnly()
    {
        return $this->activeOnly;
    }

    /**
     * @param bool $activeOnly
     * @return $this
     */
    public function setActiveOnly($activeOnly)
    {
        $this->activeOnly = $activeOnly;
        return $this;
    }

    /**
     * @return int
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param int $seats
     * @return $this
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
        return $this;
    }

    /**
     * @return $this
     */
    public function resetSeats()
    {
        $this->seats = null;
        return $this;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        $params = [];

        $params += $this->getLimitParams();
        $params += $this->getMerchantParams();
        
        if ($availabilityItemIds = $this->getIds()) {
            $params['availabilityItemId'] = $availabilityItemIds;
        }

        if ($activityIds = $this->getActivityIds()) {
            $params['activityId'] = $activityIds;
        }
        
        if ($ruleId = $this->getRuleId()) {
            $params['availabilityRuleId'] = $ruleId;
        }

        if ($startsFrom = $this->getStartsFrom()) {
            $params['startsFrom'] = $startsFrom->format('Y-m-d');
        }

        if ($startsTo = $this->getStartsTo()) {
            $params['startsTo'] = $startsTo->format('Y-m-d');
        }

        if ($bookingTime = $this->getBookingTime()) {
            $params['bookingTime'] = $bookingTime->format(DateTime::ISO8601);
        }

        if (!is_null($this->getSeats())) {
            $params['seats'] = $this->getSeats();
        }

        if ($activeOnly = $this->isActiveOnly()) {
            $params['activeOnly'] = 1;
        }

        if ($types = $this->getTypes()) {
            $params['type'] = array_values($types);
        }

        if ($this->isWithCustomPriceCategories()) {
            $params['withCustomPriceCategories'] = 1;
        }

        if (!is_null($this->getWithPriceCategoriesOnly())) {
            $params['withPriceCategoriesOnly'] = $this->getWithPriceCategoriesOnly();
        }

        if ($this->withExclusive) {
            $params['withExclusive'] = true;
        }
        if (!is_null($this->withCustom)) {
            $params['withCustom'] = $this->withCustom;
        }
        $params['withGuests'] = $this->withGuests;

        if (!is_null($this->getLanguage())) {
            $params['language'] = $this->getLanguage();
        }

        if ($departureTime = $this->getDepartureTime()) {
            $params['departureTime'] = $departureTime->format('Y-m-d');
        }

        return $params;
    }

    /**
     * @return array
     */
    public function getMixInActivities()
    {
        return $this->mixInActivities;
    }

    /**
     * @param array $mixInActivities
     * @return $this
     */
    public function setMixInActivities(array $mixInActivities)
    {
        $this->mixInActivities = $mixInActivities;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getWithPriceCategoriesOnly()
    {
        return $this->withPriceCategoriesOnly;
    }

    /**
     * @param bool|null $withPriceCategoriesOnly
     * @return $this
     */
    public function setWithPriceCategoriesOnly($withPriceCategoriesOnly)
    {
        $this->withPriceCategoriesOnly = $withPriceCategoriesOnly;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWithExclusive()
    {
        return $this->withExclusive;
    }

    /**
     * @param bool $withExclusive
     * @return $this
     */
    public function setWithExclusive($withExclusive)
    {
        $this->withExclusive = $withExclusive;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isWithCustom()
    {
        return $this->withCustom;
    }

    /**
     * @param bool $withCustom
     * @return $this
     */
    public function setWithCustom($withCustom)
    {
        $this->withCustom = $withCustom;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWithGuests()
    {
        return $this->withGuests;
    }

    /**
     * @param bool $withGuests
     * @return $this
     */
    public function setWithGuests($withGuests)
    {
        $this->withGuests = $withGuests;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string|null $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return DateTime|null
     */
    public function getDepartureTime()
    {
        return $this->departureTime;
    }

    /**
     * @param DateTime|null $departureTime
     */
    public function setDepartureTime($departureTime)
    {
        $this->departureTime = $departureTime;
    }
}
