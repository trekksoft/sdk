<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Class ActivityCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class RentalAvailableCriteria implements MerchantAwareCriteria
{
    use MerchantTrait;
    use LimitTrait;

    private $startDate;
    private $language;
    private $activityId;
    private $exactStartDate;

    /**
     * @var string
     */
    private $date;

    public function __construct(\DateTime $startDate, $activityId, $exactStartDate, $language = null)
    {
        $this->startDate = $startDate->format('Y-m-d');
        $this->language = $language;
        $this->activityId = $activityId;
        $this->exactStartDate = $exactStartDate;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        $params = [];
        $params += $this->getMerchantParams();
        $params += $this->getLimitParams();

        if ($this->startDate) {
            $params['startDate'] = $this->startDate;
        }

        if ($this->language) {
            $params['language'] = $this->language;
        }

        if ($this->activityId) {
            $params['activityId'] = [$this->activityId];
        }

        if (is_bool($this->exactStartDate)) {
            $params['exactStartDate'] = $this->exactStartDate;
        }

        return $params;
    }
}
