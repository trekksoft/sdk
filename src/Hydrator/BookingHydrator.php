<?php
namespace TrekkSoft\SDK\Hydrator;
use TrekkSoft\SDK\Model\Activity;
use TrekkSoft\SDK\Model\Booking;

/**
 * Class ActivityHydrator
 * @package TrekkSoft\SDK\Hydrator
 */
class BookingHydrator implements HydratorInterface
{
    /**
     * @param array $bookingDetails
     * @return Booking
     */
    public function hydrate(array $bookingDetails)
    {
        return new Booking($bookingDetails);
    }
}
