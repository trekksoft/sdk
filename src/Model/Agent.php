<?php
namespace TrekkSoft\SDK\Model;

class Agent
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Discount constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'id'         => null,
            'name'       => null,
            'commission' => null,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->options['name'];
    }


    /**
     * Agent commission %
     * @return string
     */
    public function getCommission()
    {
        return (float)$this->options['commission'];
    }
}
