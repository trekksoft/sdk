<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Class AvailabilityMonthCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class AvailabilityMonthCriteria implements MerchantAwareCriteria
{
    use MerchantTrait;

    /**
     * @var int
     */
    private $activityId;

    /**
     * @var string
     */
    private $startDate;

    /**
     * @var string
     */
    private $endDate;

    /**
     * @var string[]
     */
    private $types;

    public function __construct($activityId = null, \DateTime $date = null, $types = [])
    {
        $this->startDate = ($date ?: new \DateTime())->format('Y-m-01');
        $this->endDate = ($date ?: new \DateTime())->format('Y-m-t');
        $this->activityId = $activityId;
        $this->types = $types;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        $params = [];
        $params += $this->getMerchantParams();

        if ($this->activityId) {
            $params['activityId'] = [$this->activityId];
        }

        if ($this->startDate) {
            $params['startDate'] = $this->startDate;
        }

        if ($this->endDate) {
            $params['endDate'] = $this->endDate;
        }

        if ($this->types) {
            $params['type'] = $this->types;
        }

        return $params;
    }
}
