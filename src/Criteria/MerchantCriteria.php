<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Class MerchantCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class MerchantCriteria
{
    use LimitTrait;

    /**
     * @return array
     */
    /**
     * @return array
     */
    public function asArray()
    {
        $params = [];

        $params += $this->getLimitParams();

        return $params;
    }
}
