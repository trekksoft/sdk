<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\Guest;

/**
 * Class GuestsCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class GuestsCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return Guest::class;
    }
}
