<?php
namespace TrekkSoft\SDK\OAuth2\Provider;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Somoza\Psr7\OAuth2Middleware;
use TrekkSoft\SDK\Criteria\ActivityCriteria;
use TrekkSoft\SDK\Criteria\AvailabilityCriteria;
use TrekkSoft\SDK\Criteria\AvailabilityMonthCriteria;
use TrekkSoft\SDK\Criteria\BookingCriteria;
use TrekkSoft\SDK\Criteria\MerchantCriteria;
use TrekkSoft\SDK\Criteria\RentalAvailableCriteria;
use TrekkSoft\SDK\Criteria\RentalItemsCriteria;
use TrekkSoft\SDK\Criteria\RentalMonthCriteria;
use TrekkSoft\SDK\Cursor\AvailabilitiesPaginatedCursor;
use TrekkSoft\SDK\Cursor\PaginatedCursor;
use TrekkSoft\SDK\Hydrator\ActivityHydrator;
use TrekkSoft\SDK\Hydrator\AvailabilityHydrator;
use TrekkSoft\SDK\Hydrator\BookingHydrator;
use TrekkSoft\SDK\Hydrator\HydratorInterface;
use TrekkSoft\SDK\Hydrator\MerchantHydrator;
use TrekkSoft\SDK\Hydrator\RentalAvailableHydrator;
use TrekkSoft\SDK\Hydrator\RentalItemsHydrator;
use TrekkSoft\SDK\Model\Availability;

/**
 * Class TrekkSoftProvider
 * @package TrekkSoft\SDK\OAuth2\Provider
 */
class TrekkSoftProvider extends AbstractProvider
{
    protected $scheme = 'https';
    protected $host = 'api.trekksoft.com';
    protected $correlationId;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @inheritdoc
     */
    public function __construct(array $options = [], array $collaborators = [], $correlationId = null)
    {
        //Add logger
        if (isset($collaborators['logger'])) {
            if ($collaborators['logger'] instanceof LoggerInterface) {
                $this->logger = $collaborators['logger'];
            }

            unset($collaborators['logger']);
        }

        //Custom api endpoints host/scheme
        $this->host = (isset($options['host'])) ? $options['host'] : $this->host;
        unset($options['host']);

        $this->scheme = (isset($options['scheme'])) ? $options['scheme'] : $this->scheme;
        unset($options['scheme']);

        $this->correlationId = $correlationId;

        parent::__construct($options, $collaborators);

        $this->addClientMiddlewares();
    }

    /**
     * Adds middlewares to HTTP client
     */
    private function addClientMiddlewares()
    {
        $handlerStack = $this->getHttpClient()->getConfig('handler');
        if ($handlerStack instanceof HandlerStack) {
            //Log requests
            if ($this->logger) {
                $handlerStack->push(function (callable $handler) {
                    return function (RequestInterface $request, array $options) use ($handler) {
                        $this->logger->log('debug', $request->getUri());

                        return $handler($request, $options);
                    };
                });
            }

            if ($this->correlationId) {
                $handlerStack->push(function (callable $handler) {
                    return function (RequestInterface $request, array $options) use ($handler) {
                        return $handler(
                            $request->withHeader('X-Correlation-ID', $this->correlationId),
                            $options
                        );
                    };
                });
            }

            //Authorize requests
            $oauth2 = new OAuth2Middleware\Bearer($this);
            $handlerStack->push($oauth2);

            //Prefix all request uri with host and scheme
            $handlerStack->push(function (callable $handler) {
                return function (RequestInterface $request, array $options) use ($handler) {

                    $uri = $request->getUri();
                    if (!$uri->getHost()) {
                        $uri = $uri->withHost($this->host);
                    }

                    if (!$uri->getScheme()) {
                        $uri = $uri->withScheme($this->scheme);
                    }

                    $request = $request->withUri($uri);

                    return $handler($request, $options);
                };
            });
        }
    }

    /**
     * @param AvailabilityCriteria     $criteria  Criteria for availabilities request
     * @param HydratorInterface | null $hydrator  Hydrator to be used (defaults to AvailabilityHydrator)
     *
     * @return PaginatedCursor | Availability[]
     */
    public function getAvailabilities(AvailabilityCriteria $criteria = null, HydratorInterface $hydrator = null)
    {
        $criteria = $criteria ?: new AvailabilityCriteria();

        $params = $criteria->asArray();
        $query = $params ? '?'.http_build_query($params) : '';

        $request = new Request('GET', '/availabilities'.$query);
        $hydrator = $hydrator ?: new AvailabilityHydrator();

        $perPage = $criteria->getPerPage() ?: 100;

        $cursor = new AvailabilitiesPaginatedCursor($request, $this, $hydrator, $perPage);
        $cursor->setMixInActivities($criteria->getMixInActivities());

        return $cursor;
    }

    /**
     * @param BookingCriteria|null $criteria
     * @param HydratorInterface|null $hydrator
     * @return PaginatedCursor
     */
    public function getBookings(BookingCriteria $criteria = null, HydratorInterface $hydrator = null)
    {
        $criteria = $criteria ?: new BookingCriteria();

        $params = $criteria->asArray();
        $query = $params ? '?'.http_build_query($params) : '';

        $request = new Request('GET', '/bookings'.$query);
        $hydrator = $hydrator ?: new BookingHydrator();
        $perPage = $criteria->getPerPage() ?: 100;

        return new PaginatedCursor($request, $this, $hydrator, $perPage);
    }

    /**
     * @param MerchantCriteria|null $criteria
     * @param HydratorInterface|null $hydrator
     * @return PaginatedCursor
     */
    public function getMerchants(MerchantCriteria $criteria = null, HydratorInterface $hydrator = null)
    {
        $criteria = $criteria ?: new MerchantCriteria();

        $params = $criteria->asArray();
        $query = $params ? '?'.http_build_query($params) : '';

        $request = new Request('GET', '/merchants'.$query);
        $hydrator = $hydrator ?: new MerchantHydrator();
        $perPage = $criteria->getPerPage() ?: 100;

        return new PaginatedCursor($request, $this, $hydrator, $perPage);
    }

    /**
     * @param ActivityCriteria|null $criteria
     * @param HydratorInterface|null $hydrator
     * @return PaginatedCursor
     */
    public function getActivities(ActivityCriteria $criteria = null, HydratorInterface $hydrator = null)
    {
        $criteria = $criteria ?: new ActivityCriteria();

        $params = $criteria->asArray();
        $query = $params ? '?'.http_build_query($params) : '';

        $request = new Request('GET', '/activities'.$query);
        $hydrator = $hydrator ?: new ActivityHydrator();

        $perPage = $criteria->getPerPage() ?: 100;

        return new PaginatedCursor($request, $this, $hydrator, $perPage);
    }

    public function getRentalMonthDays(RentalMonthCriteria $criteria)
    {
        return $this->getAvailabilitiesMonthDays($criteria);
    }

    public function getAvailabilitiesMonthDays(AvailabilityMonthCriteria $criteria)
    {
        $params = $criteria->asArray();
        $query = $params ? '?'.http_build_query($params) : '';
        $request = new Request('GET', '/availabilities/available-days'.$query);
        return $this->getResponse($request);
    }

    public function getRentalAvailable(RentalAvailableCriteria $criteria, HydratorInterface $hydrator = null)
    {
        $params = $criteria->asArray();
        $query = $params ? '?'.http_build_query($params) : '';

        $request = new Request('GET', '/rental/available'.$query);
        $hydrator = $hydrator ?: new RentalAvailableHydrator();

        $perPage = $criteria->getPerPage() ?: 100;

        return new PaginatedCursor($request, $this, $hydrator, $perPage);
    }

    public function getRentalItems(RentalItemsCriteria $criteria, HydratorInterface $hydrator = null)
    {
        $params = $criteria->asArray();
        $query = $params ? '?'.http_build_query($params) : '';

        $hydrator = $hydrator ?: new RentalItemsHydrator();

        $request = new Request('GET', '/rental/items'.$query);
        $response = $this->getResponse($request);
        $result = [];
        foreach ($response as $item) {
            $result[] = $hydrator->hydrate($item);
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return '/oauth2/token';
    }

    /**
     * @inheritdoc
     */
    protected function getDefaultScopes()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getBaseAuthorizationUrl()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        $statusCode = $response->getStatusCode();

        if (isset($data['error']) || $statusCode<200 || $statusCode>299) {
            $error = isset($data['error']) ? $data['error'] : 'Bad status code: ' . $statusCode;
            throw new IdentityProviderException($error, $statusCode, $data);
        }
    }

    /**
     * @inheritdoc
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        return null;
    }
}
