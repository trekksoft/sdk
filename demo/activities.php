<?php
/**
 * @var TrekkSoft\SDK\OAuth2\Provider\TrekkSoftProvider $provider
 * @var \TrekkSoft\SDK\Model\Activity[] $activities
 */
$provider = require 'bootstrap.inc.php';

$criteria = new \TrekkSoft\SDK\Criteria\ActivityCriteria();
$criteria->setMerchant('bus2alps');
$criteria->setLimit(10);
$criteria->setTitleLike('rome');
$criteria->setSellingMerchant('outdoor-interlaken');

$activities = $provider->getActivities($criteria);

printf("<h2>Found Activities:</h2>");

$total = 0;
foreach ($activities as $activity) {
    printf(
        "#%d - %s (%s)<br/>",
        $activity->getId(),
        $activity->getTitle(),
        (string)$activity->getDepartureLocation()
    );
}

