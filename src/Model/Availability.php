<?php
namespace TrekkSoft\SDK\Model;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use OutOfBoundsException;
use TrekkSoft\SDK\Collection\GuestsCollection;
use TrekkSoft\SDK\Collection\PriceCategoriesCollection;

/**
 * Class Availability
 * @package TrekkSoft\SDK\Model
 */
class Availability
{
    /**
     * Availability types
     */
    const TYPE_TRIP = 'trip';
    const TYPE_ATTRACTION = 'day_trip';

    /**
     * @var array
     */
    protected $options;

    /**
     * Availability constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'merchant'           => null,
            'activityId'         => null,
            'activityTitle'      => null,
            'activity'           => [], //optional
            'availabilityItemId' => null,
            'availabilityRuleId' => null,
            'startDate'          => null,
            'startTime'          => null,
            'endDate'            => null,
            'endTime'            => null,
            'timezone'           => 'CET',
            'type'               => null,
            'availableSeats'     => 0,
            'capacity'           => 0,
            'occupancy'          => 0,
            'occupancyPaid'      => 0,
            'remarks'            => '',
            'categories'         => []
        ];

        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->options['availabilityItemId'];
    }

    /**
     * @return string
     */
    public function getMerchantCode()
    {
        return $this->options['merchant'];
    }

    /**
     * @return string
     */
    public function getAvailabilityRuleId()
    {
        return $this->options['availabilityRuleId'];
    }

    /**
     * @return int
     */
    public function getActivityId()
    {
        return (int)$this->options['activityId'];
    }

    /**
     * @return string
     */
    public function getActivityTitle()
    {
        return $this->options['activityTitle'];
    }

    /**
     * @return Activity | null
     */
    public function getActivity()
    {
        return new Activity($this->options['activity']);
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->options['startDate'];
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->options['startTime'];
    }

    /**
     * @return DateTimeInterface | null
     */
    public function getStartsAt()
    {
        if ($this->options['startDate']) {
            return new DateTimeImmutable(
                $this->options['startDate'] . ' ' . ($this->options['startTime'] ?: '00:00:00'),
                $this->getTimezone()
            );
        } else {
            return null;
        }
    }

    /**
     * @return DateTimeInterface | null
     */
    public function getEndsAt()
    {
        if ($this->options['endDate']) {
            return new DateTimeImmutable(
                $this->options['endDate'] . ' ' . ($this->options['endTime'] ?: '00:00:00'),
                $this->getTimezone()
            );
        } else {
            return null;
        }
    }

    /**
     * @return DateTimeZone
     */
    public function getTimezone()
    {
        return new DateTimeZone($this->options['timezone']);
    }

    /**
     * @return CapacityInfo
     */
    public function getCapacityInfo()
    {
        return new CapacityInfo(
            (int)$this->options['availableSeats'],
            (int)$this->options['capacity'],
            (int)$this->options['occupancy'],
            (int)$this->options['occupancyPaid']
        );
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->options['type'];
    }

    /**
     * @return string
     */
    public function getRemarks()
    {
        return $this->options['remarks'];
    }

    public function isPartnerActivity()
    {
        //@todo
        return false;
    }

    /**
     * @return PriceCategoriesCollection | PriceCategory[]
     */
    public function getPriceCategories()
    {
        $priceCategories = new PriceCategoriesCollection([]);

        foreach ($this->options['categories'] as $categoryData) {
            $priceCategory = new PriceCategory($categoryData);
            $priceCategories->add($priceCategory);
        }

        return $priceCategories;
    }

    /**
     * @return GuestsCollection | Guest[]
     */
    public function getGuests()
    {
        if (!isset($this->options['guests'])) {
            throw new OutOfBoundsException('No guest data is returned, make sure you have enough permissions');
        }

        $guests = new GuestsCollection([]);

        foreach ($this->options['guests'] as $guestData) {
            $guest = new Guest($guestData);
            $guests->add($guest);
        }

        return $guests;
    }
}
