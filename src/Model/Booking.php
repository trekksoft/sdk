<?php
namespace TrekkSoft\SDK\Model;
use Money\Currency;
use Money\Money;
use TrekkSoft\SDK\Collection\BookingItemsCollection;
use TrekkSoft\SDK\Collection\TreasurersCollection;
use TrekkSoft\SDK\Collection\VouchersCollection;
use TrekkSoft\SDK\Collection\ShopItemsCollection;
use TrekkSoft\SDK\Collection\PaymentsCollection;
use TrekkSoft\SDK\Helper\MoneyHelper;

/**
 * Class Booking
 * @package TrekkSoft\SDK\Model
 */
class Booking
{
    /**
     * Payment statuses
     */
    const PAYMENT_STATUS_EVEN        = 'even';
    const PAYMENT_STATUS_OUTSTANDING = 'outstanding';
    const PAYMENT_STATUS_OVERPAID    = 'overpaid';

    /**
     * @var array
     */
    protected $options;

    /**
     * Booking constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'id'                => null,
            'merchant'          => null,
            'currency'          => null,
            'conversionRate'    => null,
            'createdAt'         => null,
            'items'             => [],
            'total'             => 0,
            'itemsPrice'        => 0,
            'shopItemsPrice'    => 0,
            'vouchers'          => [],
            'vouchersDiscount'  => 0,
            'multidiscount'     => 0,
            'shopItems'         => [],
            'treasurers'        => [],
            'user' => [
                'id'    => null,
                'name'  => null,
                'email' => null,
            ],
            'agent'             => null,
            'payments'          => [],
            'paymentStatus'     => null,
            'status'            => null,
            'isReservation'     => false,
            'subTotal'          => null,
            'fullCaption'       => null,
            'onlineBookingFee'  => null,
            'totalFees'         => null,
            'balance'           => null,
            'totalPayments'     => null,
            'totalRefunds'      => null,
            'totalDiscounts'    => null,
            'rebates'           => null,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->options['id'] ? (int)$this->options['id'] : null;
    }


    /**
     * @return Money
     */
    public function getSubTotal()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['subTotal'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return string
     */
    public function getFullCaption()
    {
        return $this->options['fullCaption'];
    }

    /**
     * @return Money
     */
    public function getOnlineBookingFee()
    {
       return MoneyHelper::createFromArray([
           'amount' => $this->options['onlineBookingFee'],
           'currency' => $this->options['currency'],
       ]);
    }

    /**
     * @return Money
     */
    public function getTotalFees()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['totalFees'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getBalance()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['balance'],
            'currency' => $this->options['currency'],
        ]);
    }


    /**
     * @return Money
     */
    public function getTotalPayments()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['totalPayments'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getTotalRefunds()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['totalRefunds'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getTotalDiscounts()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['totalDiscounts'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getTotalRebates()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['rebates'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getTotal()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['total'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return new Currency($this->options['currency']);
    }

    /**
     * @return BookingItemsCollection | BookingItem[]
     */
    public function getItems()
    {
        $bookingItems = new BookingItemsCollection([]);

        foreach ($this->options['items'] as $bookingItemData) {
            $bookingItemData['currency'] = $this->options['currency'];
            $bookingItem = new BookingItem($bookingItemData);
            $bookingItems->add($bookingItem);
        }

        return $bookingItems;
    }

    /**
     * @return Money
     */
    public function getItemsPrice()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['itemsPrice'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getShopItemsPrice()
    {
        return MoneyHelper::createFromArray([
            'amount'   => $this->options['shopItemsPrice'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return VouchersCollection
     */
    public function getVouchers()
    {
        $vouchersCollection = new VouchersCollection([]);

        foreach($this->options['vouchers'] as $voucherData) {
            $voucherData['currency'] = $this->getCurrency()->getName();
            $vouchersCollection->add(new Voucher($voucherData));
        }

        return $vouchersCollection;
    }

    /**
     * @return Money
     */
    public function getVouchersDiscount()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['vouchersDiscount'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getPackageDiscount()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['multidiscount'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return ShopItemsCollection
     */
    public function getShopItems()
    {
        $shopItemsCollection = new ShopItemsCollection([]);

        foreach($this->options['shopItems'] as $shopItemData) {
            $shopItemData['currency'] = $this->getCurrency()->getName();
            $shopItemsCollection->add(new ShopItem($shopItemData));
        }

        return $shopItemsCollection;
    }

    /**
     * @return float
     */
    public function getConversionRate()
    {
        return (float)$this->options['conversionRate'];
    }

    /**
     * @return string
     */
    public function getMerchantCode()
    {
        return $this->options['merchant'];
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt()
    {
        return new \DateTimeImmutable($this->options['createdAt']);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return new User($this->options['user']);
    }

    /**
     * @return Agent
     */
    public function getAgent()
    {
        return $this->options['agent'] ? new Agent($this->options['agent']) : null;
    }
    
    /**
     * @return PaymentsCollection
     */
    public function getPayments()
    {
        $paymentsCollection = new PaymentsCollection([]);

        foreach($this->options['payments'] as $paymentData) {
            $paymentData['currency'] = $this->getCurrency()->getCode();
            $paymentsCollection->add(new Payment($paymentData));
        }

        return $paymentsCollection;
    }

    /**
     * @return string
     */
    public function getPaymentStatus()
    {
        return $this->options['paymentStatus'];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->options['status'];
    }

    /**
     * @return bool
     */
    public function isReservation()
    {
        return (bool)$this->options['isReservation'];
    }

    /**
     * @return TreasurersCollection | Treasurer[]
     */
    public function getTreasurers()
    {
        $treasurersCollection = new TreasurersCollection([]);

        foreach($this->options['treasurers'] as $treasurerData) {
            $treasurerData['currency'] = $this->getCurrency()->getName();
            $treasurersCollection->add(new Treasurer($treasurerData));
        }

        return $treasurersCollection;
    }
}
