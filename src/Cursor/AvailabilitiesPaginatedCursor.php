<?php
namespace TrekkSoft\SDK\Cursor;


use TrekkSoft\SDK\Criteria\ActivityCriteria;
use TrekkSoft\SDK\Hydrator\ArrayHydrator;

class AvailabilitiesPaginatedCursor extends PaginatedCursor
{
    /**
     * @var array
     */
    protected $mixInActivities = [];

    /**
     * @param array $mixInActivities
     */
    public function setMixInActivities(array $mixInActivities)
    {
        $this->mixInActivities = $mixInActivities;
    }

    /**
     * @return array
     */
    protected function processNextDataSet()
    {
        $nextDataSet = parent::processNextDataSet();

        if ($this->mixInActivities) {
            $nextDataSet = $this->mixInActivities($nextDataSet, $this->mixInActivities);
        }

        return $nextDataSet;
    }

    /**
     * Mixes in corresponding activity data into each availability
     *
     * @param array $availabilities
     * @param array $activities
     * @return array
     */
    protected function mixInActivities(array $availabilities, array $activities)
    {
        //Mix in corresponding activity data into each availability
        foreach ($availabilities as &$availability) {
            $availability['activity'] = [];

            if (isset($availability['activityId'])) {
                $activityId = (int)$availability['activityId'];
                if (isset($activities[$activityId])) {
                    $availability['activity'] = $activities[$activityId];
                }
            }
        }

        //Reset availabilities data set
        reset($availabilities);

        return $availabilities;
    }
}
