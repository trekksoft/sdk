<?php
namespace TrekkSoft\SDK\Hydrator;

/**
 * Class ArrayHydrator
 * @package TrekkSoft\SDK\Hydrator
 */
class ArrayHydrator implements HydratorInterface
{
    /**
     * Simple hydration (return item as is)
     *
     * @param array $item
     * @return array
     */
    public function hydrate(array $item)
    {
        return $item;
    }
}
