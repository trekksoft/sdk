<?php
namespace TrekkSoft\SDK\Hydrator;
use TrekkSoft\SDK\Model\RentalItem;

/**
 * Class RentalAvailableHydrator
 * @package TrekkSoft\SDK\Hydrator
 */
class RentalItemsHydrator implements HydratorInterface
{
    /**
     * @param array $item
     * @return RentalItem
     */
    public function hydrate(array $item)
    {
        return new RentalItem($item);
    }
}
