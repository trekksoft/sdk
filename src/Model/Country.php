<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class Country
 * @package TrekkSoft\SDK\Model
 */
class Country
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Country constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'code'  => null,
            'name'  => null,
        ];

        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->options['code'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->options['name'];
    }
}
