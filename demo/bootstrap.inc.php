<?php
ini_set('display_errors', 1);
include '../vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$credentials = require 'config.inc.php';

$logger = new Logger('access');
$logger->pushHandler(new StreamHandler('../logs/access.log'));

$provider = new \TrekkSoft\SDK\OAuth2\Provider\TrekkSoftProvider($credentials, [
    'logger' => $logger,
]);

return $provider;
