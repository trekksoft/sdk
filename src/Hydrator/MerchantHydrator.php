<?php
namespace TrekkSoft\SDK\Hydrator;

/**
 * Class MerchantHydrator
 * @package TrekkSoft\SDK\Hydrator
 */
class MerchantHydrator implements HydratorInterface
{
    public function hydrate(array $item)
    {
        throw new \RuntimeException('Not implemented yet, use ArrayHydrator for now instead');
    }
}
