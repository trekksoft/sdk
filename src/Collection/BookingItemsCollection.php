<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\BookingItem;

/**
 * Class BookingItemsCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class BookingItemsCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return BookingItem::class;
    }
}
