<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class City
 * @package TrekkSoft\SDK\Model
 */
class City
{
    /**
     * @var array
     */
    protected $options;

    /**
     * City constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'id'        => null,
            'locode'    => null,
            'name'      => null,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return string
     */
    public function getLocode()
    {
        return $this->options['locode'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->options['name'];
    }
}
