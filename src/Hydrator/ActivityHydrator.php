<?php
namespace TrekkSoft\SDK\Hydrator;
use TrekkSoft\SDK\Model\Activity;

/**
 * Class ActivityHydrator
 * @package TrekkSoft\SDK\Hydrator
 */
class ActivityHydrator implements HydratorInterface
{
    /**
     * @param array $item
     * @return Activity
     */
    public function hydrate(array $item)
    {
        return new Activity($item);
    }
}
