<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Trait MerchantTrait
 * @package TrekkSoft\SDK\Criteria
 */
trait MerchantTrait
{
    /**
     * @var string[]
     */
    private $merchants = [];

    /**
     * @var string[]
     */
    private $excludeMerchants = [];

    /**
     * @var string[]
     */
    private $sellingMerchants = [];

    /**
     * @param string $merchant
     * @return $this
     */
    public function addMerchant($merchant)
    {
        $this->merchants[$merchant] = $merchant;
        return $this;
    }

    /**
     * @param string $merchant
     * @return $this
     */
    public function setMerchant($merchant)
    {
        $this->merchants = [];
        if ($merchant) {
            $this->addMerchant($merchant);
        }
        return $this;
    }

    /**
     * @return string[]
     */
    public function getMerchants()
    {
        return array_values($this->merchants);
    }

    /**
     * @param string $sellingMerchant
     * @return $this
     */
    public function addSellingMerchant($sellingMerchant)
    {
        $this->sellingMerchants[$sellingMerchant] = $sellingMerchant;
        return $this;
    }

    /**
     * @param string $sellingMerchant
     * @return $this
     */
    public function setSellingMerchant($sellingMerchant)
    {
        $this->sellingMerchants = [];
        if ($sellingMerchant) {
            $this->addSellingMerchant($sellingMerchant);
        }
        return $this;
    }

    /**
     * @param string $excludeMerchant
     * @return $this
     */
    public function addExcludeMerchant($excludeMerchant)
    {
        $this->excludeMerchants[$excludeMerchant] = $excludeMerchant;
        return $this;
    }

    /**
     * @param string $excludeMerchant
     * @return $this
     */
    public function setExcludeMerchant($excludeMerchant)
    {
        $this->excludeMerchants = [];
        if ($excludeMerchant) {
            $this->addExcludeMerchant($excludeMerchant);
        }
        return $this;
    }

    /**
     * @return string[]
     */
    public function getExcludeMerchants()
    {
        return array_values($this->excludeMerchants);
    }

    /**
     * @return string[]
     */
    public function getSellingMerchants()
    {
        return array_values($this->sellingMerchants);
    }    

    /**
     * @return array
     */
    private function getMerchantParams()
    {
        $params = [];

        if ($merchants = $this->getMerchants()) {
            $params['merchant'] = $merchants;
        }

        if ($sellingMerchants = $this->getSellingMerchants()) {
            $params['sellingMerchant'] = $sellingMerchants;
        }

        if ($excludeMerchants = $this->getExcludeMerchants()) {
            $params['excludeMerchant'] = $excludeMerchants;
        }
        
        return $params;
    }
}
