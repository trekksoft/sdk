<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class Activity
 * @package TrekkSoft\SDK\Model
 */
class RentalItem
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Activity constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'id'        => null,
            'startDate' => null,
            'startTime' => null,
            'endDate'   => null,
            'endTime'   => null,
            'duration'  => null,
        ];

        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->options['availabilityItemId'];
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->options['startDate'];
    }

    /**
     * @return string
     */
    public function getStartTime()
    {
        return $this->options['startTime'];
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->options['endDate'];
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->options['endTime'];
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->options['duration'];
    }
}
