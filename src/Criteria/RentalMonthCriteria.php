<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Class RentalMonthCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class RentalMonthCriteria extends AvailabilityMonthCriteria {
    public function asArray()
    {
        $params = parent::asArray();
        $params['type'] = ['rental'];

        return $params;
    }
}