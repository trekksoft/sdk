<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class User
 * @package TrekkSoft\SDK\Model
 */
class User
{
    /**
     * @var array
     */
    protected $options;

    /**
     * User constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'id'    => null,
            'name'  => null,
            'email' => null,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->options['name'];
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->options['email'];
    }
}
