<?php
namespace TrekkSoft\SDK\Cursor;

use League\OAuth2\Client\Provider\AbstractProvider;
use Psr\Http\Message\RequestInterface;
use Iterator;
use TrekkSoft\SDK\Hydrator\ArrayHydrator;
use TrekkSoft\SDK\Hydrator\HydratorInterface;

/**
 * Class PaginatedCursor
 * @package TrekkSoft\SDK\Cursor
 */
class PaginatedCursor implements Iterator
{
    /**
     * @var RequestInterface
     */
    private $firstRequest;

    /**
     * @var RequestInterface
     */
    private $nextRequest;

    /**
     * @var AbstractProvider
     */
    private $provider;

    /**
     * @var array
     */
    private $currentDataSet = [];

    /**
     * @var int
     */
    private $offset = 0;

    /**
     * @var int
     */
    private $nbCurrentFetched = 0;

    /**
     * @var int
     */
    private $hardLimit = null;

    /**
     * @var int
     */
    private $perPage = 100;

    /**
     * @var int
     */
    private $nbTotalFetched = 0;

    /**
     * @var HydratorInterface
     */
    private $hydrator;

    /**
     * PaginatedCursor constructor.
     * @param RequestInterface  $request
     * @param AbstractProvider  $provider
     * @param HydratorInterface $hydrator
     * @param int               $perPage
     */
    public function __construct(RequestInterface $request, AbstractProvider $provider, HydratorInterface $hydrator = null, $perPage = 100)
    {
        $this->firstRequest = $request; //store it to be able to rewind
        $this->provider = $provider;
        $this->perPage = $perPage;
        $this->hydrator = $hydrator ?: new ArrayHydrator();

        $this->nextRequest = $this->prepareNextRequest($request);
    }

    /**
     * @return bool|mixed
     */
    public function next()
    {
        $next = $this->currentDataSet ? next($this->currentDataSet) : false;

        if ($next === false) {
            $dataSet = $this->getNextDataSet();
            if ($dataSet !== null) {
                $next = current($dataSet);
            }
        }

        return $this->hydrate($next);
    }

    /**
     * @return bool|mixed
     */
    public function current()
    {
        $dataSet = $this->getCurrentDataSet();
        $current = $dataSet ? current($dataSet) : false;

        return $this->hydrate($current);
    }

    /**
     * Rewind
     */
    public function rewind()
    {
        $this->currentDataSet = [];
        $this->nextRequest = $this->prepareNextRequest($this->firstRequest);
        $this->nbTotalFetched = 0;
        $this->nbCurrentFetched = 0;
    }

    /**
     * @return bool|int
     */
    public function key()
    {
        $dataSet = $this->getCurrentDataSet();

        return $dataSet ? ($this->getPosition() + key($dataSet)) : false;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return $this->getCurrentDataSet() ? true : false;
    }

    /**
     * @return array
     */
    protected function getCurrentDataSet()
    {
        if (!$this->currentDataSet) {
            $this->currentDataSet = $this->getNextDataSet();
        }

        return $this->currentDataSet;
    }

    /**
     * @param RequestInterface $request
     * @return RequestInterface
     */
    private function prepareNextRequest(RequestInterface $request)
    {
        //Query string into array
        parse_str($request->getUri()->getQuery(), $params);

        //Determine hard limit and starting offset on first request only
        if ($this->nbTotalFetched === 0) {
            $this->hardLimit = isset($params['limit']) ? (int)$params['limit'] : null;
            $this->offset = isset($params['offset']) ? (int)$params['offset'] : 0;
        }
        
        $limit = ($this->hardLimit !== null) ? min($this->perPage, $this->hardLimit) : $this->perPage;
        $offset = $this->offset + count($this->currentDataSet);
        $this->offset = $offset;

        if ($this->hardLimit !== null) {
            $limit = max(0, min($this->hardLimit - $this->nbTotalFetched, $this->perPage));
        }

        if ($limit > 0) {
            $params['limit'] = $limit;
            $params['offset'] = $offset;

            $uri = $request->getUri()->withQuery(http_build_query($params));

            $this->nextRequest = $request->withUri($uri);
        } else {
            $this->nextRequest = null;
        }

        return $this->nextRequest;
    }

    /**
     * @return array
     */
    protected function getNextDataSet()
    {
        if ($this->nextRequest) {
            $this->currentDataSet = $this->processNextDataSet();
            $count = count($this->currentDataSet);

            $this->nbCurrentFetched = $count;
            $this->nbTotalFetched += $count;

            if ($count > 0 && !$this->isLastPageLoaded()) {
                $this->nextRequest = $this->prepareNextRequest($this->nextRequest);
            } else {
                $this->nextRequest = null;
            }
        } else {
            $this->currentDataSet = [];
            $this->nextRequest = null;
        }

        return $this->currentDataSet;
    }

    protected function isLastPageLoaded()
    {
        parse_str($this->nextRequest->getUri()->getQuery(), $params);

        if ($this->nbTotalFetched === 0) {
            $this->hardLimit = isset($params['limit']) ? (int)$params['limit'] : null;
        }

        $limit = ($this->hardLimit !== null) ? min($this->perPage, $this->hardLimit) : $this->perPage;

        return $limit != count($this->currentDataSet);
    }

    /**
     * @return array
     */
    protected function processNextDataSet()
    {
        $currentDataSet = $this->provider->getResponse($this->nextRequest);

        return $currentDataSet;
    }

    /**
     * @param mixed $input
     * @return mixed|bool
     */
    protected function hydrate($input)
    {
        if ($input === false) {
            return false;
        } else {
            return $this->hydrator->hydrate($input);
        }
    }

    /**
     * @return int
     */
    protected function getPosition()
    {
        return max(0, $this->nbTotalFetched - $this->nbCurrentFetched);
    }
}
