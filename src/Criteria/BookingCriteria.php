<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Class BookingCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class BookingCriteria implements MerchantAwareCriteria
{
    use LimitTrait;
    use MerchantTrait;

    /**
     * @return array
     */
    public function asArray()
    {
        $params = [];

        $params += $this->getLimitParams();
        $params += $this->getMerchantParams();

        return $params;
    }
}
