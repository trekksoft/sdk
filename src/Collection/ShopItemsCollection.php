<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\ShopItem;

/**
 * Class ShopItemsCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class ShopItemsCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return ShopItem::class;
    }
}
