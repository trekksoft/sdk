<?php
namespace TrekkSoft\SDK\Model;

use Money\Money;
use TrekkSoft\SDK\Helper\MoneyHelper;

/**
 * Class Addon
 * @package TrekkSoft\SDK\Model
 */
class Addon
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Addon constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'id'        => null,
            'title'     => null,
            'canceled' => false,
            'price'     => [
                'amount' => null,
                'currency' => null,
            ],
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->options['title'];
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return (bool)$this->options['canceled'];
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return MoneyHelper::createFromArray($this->options['price']);
    }
}
