<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Class ActivityCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class RentalItemsCriteria implements MerchantAwareCriteria
{
    use MerchantTrait;

    private $startDate;
    private $language;
    private $ruleId;
    private $duration;

    /**
     * @var string
     */
    private $date;

    public function __construct(\DateTime $startDate, $ruleId, $duration, $language = null)
    {
        $this->startDate = $startDate->format('Y-m-d');
        $this->language = $language;
        $this->ruleId = $ruleId;
        $this->duration = $duration;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        $params = [];
        $params += $this->getMerchantParams();

        if ($this->startDate) {
            $params['startDate'] = $this->startDate;
        }

        if ($this->language) {
            $params['language'] = $this->language;
        }

        if ($this->ruleId) {
            $params['ruleId'] = $this->ruleId;
        }

        if ($this->duration) {
            $params['duration'] = $this->duration;
        }

        return $params;
    }
}
