<?php
namespace TrekkSoft\SDK\Hydrator;

/**
 * Interface HydratorInterface
 * Hydrator is something that converts data from array into suitable representation (e.g. object)
 *
 * @package TrekkSoft\SDK\Hydrator
 */
interface HydratorInterface
{
    /**
     * @param array $item
     * @return mixed
     */
    public function hydrate(array $item);
}
